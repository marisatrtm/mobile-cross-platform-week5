import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Quote } from '../../data/quote.interface';
import { QuotesService } from '../../services/quotes';
import { QuotePage } from '../quote/quote';
import { SettingsService } from '../../services/settings';

/**
 * Generated class for the FavoritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage{

  quoteCollection:Quote[];
  constructor( private quotesService:QuotesService, public navCtrl: NavController, public navParams: NavParams,
  private modalCtrl:ModalController, private settingsSvc: SettingsService) {
  }

  ionViewWillEnter(){
   this.quoteCollection=this.quotesService.getFavoriteQuotes();
  }

  showQuote(quote:Quote){
    console.log(quote);
    let modal = this.modalCtrl.create(QuotePage,{quoteText:quote.text, quotePerson: quote.person});
    modal.present();
    modal.onDidDismiss(
      (remove:boolean)=>{
        if(remove){
          this.quotesService.removeQuoteFromFavorites(quote);
          console.log(this.quoteCollection);
          this.quoteCollection = this.quotesService.getFavoriteQuotes();
        }
      }
    );
  }

  removeQuote(quote:Quote){
    this.quotesService.removeQuoteFromFavorites(quote);
    this.quoteCollection= this.quotesService.getFavoriteQuotes();
  }

  setBgColor(){
    return this.settingsSvc.isAltBackground() ? 'altQuoteBg' : 'altQuoteBg2';
  }

}
